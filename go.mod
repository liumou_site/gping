module gitee.com/liumou_site/gping

go 1.19

require (
	gitee.com/liumou_site/gbm v1.0.5
	gitee.com/liumou_site/gf v1.2.12
	gitee.com/liumou_site/gns v1.3.2
	gitee.com/liumou_site/logger v1.2.0
	github.com/spf13/cast v1.5.1
)

require (
	gitee.com/liumou_site/gcs v1.8.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.9.0 // indirect
)
