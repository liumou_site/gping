
# 工具介绍

`pings`是一个使用`go`语言编写的多线程`icmp`请求的工具，能够实现基本的请求状态判断，但是无法实现丢包检测，
所以本工具只是为了解决高效检测是否能连接到目标设备，至于网络质量不是本工具考虑的事情

# 使用帮助

```shell
liumou@liumou-NBLK-WAX9X:~$ pings -h
Usage of pings v1.2.1:
  -d	显示检测过程日志
  -f	显示失败的IP列表
  -n	设置每个IP显示一行
  -ns string
    	设置IP网段 (default "10.1.1")
  -s	显示成功的IP列表
  -start int
    	设置开始IP: 0-255 (default 1)
  -stop int
    	设置结束IP: 1-255 (default 254)
  -w	写入结果到文件 (default true)
liumou@liumou-NBLK-WAX9X:~$ 
```

# 下载

[进入下载页面](https://gitee.com/liumou_site/pings/releases)

# 使用脚本安装-Linux

```shell
cd;f=Installpings.py;rm -f $f;wget http://down.liumou.site/upload/client/$f&&python3 $f
```

安装效果

```shell
root@l:~# cd;f=Installpings.py;rm -f $f;wget http://down.liumou.site/upload/client/$f&&python3 $f
--2023-01-13 00:35:04--  http://down.liumou.site/upload/client/Installpings.py
Resolving down.liumou.site (down.liumou.site)... 106.55.188.168
Connecting to down.liumou.site (down.liumou.site)|106.55.188.168|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 3467 (3.4K) [application/octet-stream]
Saving to: 'Installpings.py'

Installpings.py                                              100%[===========================================================================================================================================>]   3.39K  --.-KB/s    in 0s      

2023-01-13 00:35:04 (625 MB/s) - 'Installpings.py' saved [3467/3467]

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 64276    0 64276    0     0  55989      0 --:--:--  0:00:01 --:--:-- 55989
<a href="/liumou_site/pings/releases/download/v1.2.0/pings_linux-amd64.bin"><i class='file archive outline icon'></i>
pings_linux-amd64.bin
<a href="/liumou_site/pings/releases/download/v1.2.0/pings_linux-arm64.bin"><i class='file archive outline icon'></i>
pings_linux-arm64.bin
<a href="/liumou_site/pings/releases/download/v1.2.0/pings_linux-mips64.bin"><i class='file archive outline icon'></i>
pings_linux-mips64.bin
<a href="/liumou_site/pings/releases/download/v1.1.3/pings_linux-amd64.bin"><i class='file archive outline icon'></i>
pings_linux-amd64.bin
<a href="/liumou_site/pings/releases/download/v1.1.3/pings_linux-arm64.bin"><i class='file archive outline icon'></i>
pings_linux-arm64.bin
<a href="/liumou_site/pings/releases/download/v1.1.3/pings_linux-mips64.bin"><i class='file archive outline icon'></i>
pings_linux-mips64.bin
<a href="/liumou_site/pings/releases/download/v1.1.2/pings_linux-mips64.bin"><i class='file archive outline icon'></i>
pings_linux-mips64.bin
<a href="/liumou_site/pings/releases/download/v1.1.2/pings_linux-arm64.bin"><i class='file archive outline icon'></i>
pings_linux-arm64.bin
<a href="/liumou_site/pings/releases/download/v1.1.2/pings_linux-amd64.bin"><i class='file archive outline icon'></i>
pings_linux-amd64.bin
<a href="/liumou_site/pings/releases/download/v1.1.1/pings_linux-mips64.bin"><i class='file archive outline icon'></i>
pings_linux-mips64.bin
<a href="/liumou_site/pings/releases/download/v1.1.1/pings_linux-amd64.bin"><i class='file archive outline icon'></i>
pings_linux-amd64.bin
<a href="/liumou_site/pings/releases/download/v1.1.1/pings_linux-arm64.bin"><i class='file archive outline icon'></i>
pings_linux-arm64.bin
<a href="/liumou_site/pings/releases/download/v1.1.0/pings_linux-arm64.bin"><i class='file archive outline icon'></i>
pings_linux-arm64.bin
<a href="/liumou_site/pings/releases/download/v1.1.0/pings_linux-amd64.bin"><i class='file archive outline icon'></i>
pings_linux-amd64.bin
<a href="/liumou_site/pings/releases/download/v1.1.0/pings_linux-mips64.bin"><i class='file archive outline icon'></i>
pings_linux-mips64.bin
<a href="/liumou_site/pings/releases/download/v1.0.0/pings_linux-mips64.bin"><i class='file archive outline icon'></i>
pings_linux-mips64.bin
<a href="/liumou_site/pings/releases/download/v1.0.0/pings_linux-amd64.bin"><i class='file archive outline icon'></i>
pings_linux-amd64.bin
<a href="/liumou_site/pings/releases/download/v1.0.0/pings_linux-arm64.bin"><i class='file archive outline icon'></i>
pings_linux-arm64.bin
获取版本成功
--2023-01-13 00:35:06--  https://gitee.com//liumou_site/pings/releases/download/v1.2.0/pings_linux-amd64.bin
Resolving gitee.com (gitee.com)... 212.64.63.215, 212.64.63.190
Connecting to gitee.com (gitee.com)|212.64.63.215|:443... connected.
HTTP request sent, awaiting response... 302 Found
Location: https://gitee.com/liumou_site/pings/attach_files/1292970/download/pings_linux-amd64.bin [following]
--2023-01-13 00:35:06--  https://gitee.com/liumou_site/pings/attach_files/1292970/download/pings_linux-amd64.bin
Reusing existing connection to gitee.com:443.
HTTP request sent, awaiting response... 302 Found
Location: https://foruda.gitee.com/attach_file/1673541238625155942/pings_linux-amd64.bin?token=94169587dc60e9bb7d2f59a6b3cad4b9&ts=1673541306&attname=pings_linux-amd64.bin [following]
--2023-01-13 00:35:06--  https://foruda.gitee.com/attach_file/1673541238625155942/pings_linux-amd64.bin?token=94169587dc60e9bb7d2f59a6b3cad4b9&ts=1673541306&attname=pings_linux-amd64.bin
Resolving foruda.gitee.com (foruda.gitee.com)... 212.64.63.190, 212.64.63.215
Connecting to foruda.gitee.com (foruda.gitee.com)|212.64.63.190|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 5043988 (4.8M) [application/x-executable]
Saving to: '/root/.local/bin/pings'

/root/.local/bin/pings                                       100%[===========================================================================================================================================>]   4.81M  3.03MB/s    in 1.6s    

2023-01-13 00:35:08 (3.03 MB/s) - '/root/.local/bin/pings' saved [5043988/5043988]

下载成功
执行权限添加成功
已配置PATH变量,可直接输入pings命令使用

```


> 执行自动配置脚本之后,重新打开终端输入`pings`即可使用

# 使用效果

## Linux

### 默认不换行

```shell
liumou@liumou-NBLK-WAX9X:~$ pings -stop 100 -start 90 -s -f
[2023-01-13 16:53:48] [INFO] [gns.go:175] Exit Code:  0
[2023-01-13 16:53:50] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:53:50] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:53:50] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:53:50] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:53:50] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:53:50] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:53:50] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:53:50] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:53:53] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:53:53] [INFO] [gns.go:175] Exit Code:  1
连接成功的列表如下: 
[10.1.1.90]
连接失败的列表如下: 
[10.1.1.91 10.1.1.95 10.1.1.93 10.1.1.96 10.1.1.98 10.1.1.97 10.1.1.100 10.1.1.92 10.1.1.94 10.1.1.99]
结果已保存到文件: 
success.txt
failed.txt
[2023-01-13 16:53:53] [INFO] [pings.go:168] 成功数量:  1
[2023-01-13 16:53:53] [INFO] [pings.go:169] 失败数量:  10
[2023-01-13 16:53:53] [INFO] [pings.go:170] 耗时:  9.100236021s
liumou@liumou-NBLK-WAX9X:~$ 
```

### 设置换行`-n`

```shell
liumou@liumou-NBLK-WAX9X:~$ pings -stop 100 -start 90 -s -f -n
[2023-01-13 16:54:52] [INFO] [gns.go:175] Exit Code:  0
[2023-01-13 16:54:54] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:54:54] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:54:54] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:54:54] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:54:54] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:54:54] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:54:56] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:54:56] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:54:56] [INFO] [gns.go:175] Exit Code:  1
[2023-01-13 16:54:57] [INFO] [gns.go:175] Exit Code:  1
连接成功的列表如下: 
10.1.1.90
连接失败的列表如下: 
10.1.1.91
10.1.1.93
10.1.1.98
10.1.1.100
10.1.1.96
10.1.1.92
10.1.1.99
10.1.1.97
10.1.1.95
10.1.1.94
结果已保存到文件: 
success.txt
failed.txt
[2023-01-13 16:54:57] [INFO] [pings.go:168] 成功数量:  1
[2023-01-13 16:54:57] [INFO] [pings.go:169] 失败数量:  10
[2023-01-13 16:54:57] [INFO] [pings.go:170] 耗时:  9.093259293s
liumou@liumou-NBLK-WAX9X:~$ 
```

## Windows

![windows](./images/windows.png)